#include "llvm/IR/DerivedTypes.h"
#include "llvm/IR/Verifier.h"
#include "llvm/IR/IRBuilder.h"
#include "llvm/IR/LLVMContext.h"
#include "llvm/IR/Module.h"
#include "llvm/IR/Function.h"
#include "llvm/IR/Type.h"
#include "llvm/IR/Value.h"
#include "llvm/Transforms/InstCombine/InstCombine.h"
#include "llvm/Transforms/Scalar.h"
#include "llvm/Transforms/Scalar/GVN.h"
#include "llvm/Transforms/Utils.h"
#include "llvm/Support/TargetSelect.h"
#include "llvm/Support/FileSystem.h"
#include "llvm/Support/Host.h"
#include "llvm/Support/raw_ostream.h"
#include "llvm/Support/TargetRegistry.h"
#include "llvm/Target/TargetMachine.h"
#include "llvm/Target/TargetOptions.h"
#include "llvm/IR/LegacyPassManager.h"

#include <vector>
#include <map>
#include <string>

using namespace llvm;

using std::vector;
using std::string;
using std::map;
using std::unique_ptr;
using std::make_unique;

static LLVMContext Context;
static IRBuilder<> Builder(Context);
static unique_ptr<Module> TheModule;
static map<string, Value*> NamedValues;


// 相当于为下面的程序生成IR：
// int fun1(int a, int b){
//     int c = a + b;
//     if (c >= 13)
//          c = 26;
//          return c;
//     else
//          c = a - b;
//          return c;
// }
Function* codegen_fun1() {
    // Declare function
    // 2 * int32
    vector<Type*> argType(2, Type::getInt32Ty(Context));
    FunctionType* funDec = FunctionType::get(
        Type::getInt32Ty(Context),  // return type
        argType,                    // args type
        false                       // var len arg?
    );

    // Impl function
    Function* fun = Function::Create(
        funDec,
        Function::ExternalLinkage,
        "fun1",
        TheModule.get()
    );

    // Set arg Name
    // vector<string> argName = {"a", "b"};
    string argName[2] = {"a", "b"};
    unsigned i = 0;
    for (auto &arg : fun->args()) {
        arg.setName(argName[i++]);
    }

    // Create a basic block
    BasicBlock *RootBlk = BasicBlock::Create(Context, "", fun);
    Builder.SetInsertPoint(RootBlk);

    NamedValues.clear();
    for(auto &Arg : fun->args())
        NamedValues[string(Arg.getName())] = &Arg;

    Value* a = NamedValues["a"];
    Value* b = NamedValues["b"];

    Value* sum = Builder.CreateAdd(a, b);

    // int c = a + b
    // inherrit from Value
    AllocaInst* c = Builder.CreateAlloca(
        Type::getInt32Ty(Context),
        nullptr,
        "c"
    );
    // init value(0) (similiar machanism with global variable's .bss section)
    // Value* initValue = ConstantInt::get(TheContext, APInt(32, 0, true));
    Builder.CreateStore(sum, c);

    // Condition Stmt
    // UGE (un great equal)

    Value * CondV1 = sum;
    Value * CondV2 = ConstantInt::get(Context, APInt(32, 13, true));
    Value* CondRes = Builder.CreateICmpUGE(CondV1, CondV2, "cmp_res");

    // if/else block
    BasicBlock *ThenBlk =BasicBlock::Create(Context, "then", fun);  // 这里指定了fun, 就不需要再插入
    BasicBlock *ElseBlk = BasicBlock::Create(Context, "else");
    BasicBlock *MergeBlk = BasicBlock::Create(Context, "ifcont");

    Builder.CreateCondBr(CondRes, ThenBlk, ElseBlk);

    // then => merge
    Builder.SetInsertPoint(ThenBlk);
    // then c = 26
    Builder.CreateStore(
        ConstantInt::get(Context, APInt(32, 26, true)),  // 26 as signed 32,
        c
    );
    Builder.CreateBr(MergeBlk);

    // insert else && connect else => merge
    fun->getBasicBlockList().push_back(ElseBlk);
    Builder.SetInsertPoint(ElseBlk);
    // else c = a - b
    Builder.CreateStore(
        Builder.CreateSub(a, b),
        c
    );
    Builder.CreateBr(MergeBlk);

    // insert merge
    fun->getBasicBlockList().push_back(MergeBlk);
    Builder.SetInsertPoint(MergeBlk);


    Builder.CreateRet(c);

    verifyFunction(*fun);

    return fun;
}

#ifdef _WIN32
#define DLLEXPORT __declspec(dllexport)
#else
#define DLLEXPORT
#endif
extern "C" DLLEXPORT void printInt(int a){
    printf("%d\n",a);
}

Function* codegen_main() {
    FunctionType* mainDec = FunctionType::get(
        Type::getInt32Ty(Context),
        false
    );

    Function* main = Function::Create(
        mainDec,
        Function::ExternalLinkage,
        "__start",
        TheModule.get()
    );

    BasicBlock* MainBlk = BasicBlock::Create(Context, "", main);
    Builder.SetInsertPoint(MainBlk);

    Value* argValue[2] = {
        ConstantInt::get(Context, APInt(32, 2, true)),
        ConstantInt::get(Context, APInt(32, 3, true))
    };

    Function* callee = TheModule->getFunction("fun1");
    Value* rtn = Builder.CreateCall(callee, argValue, "calltmp");

    // 调用外部print
    vector<Type *> printIntArgType(1, Type::getInt32Ty(Context));
    FunctionType *printIntDec = FunctionType::get(
        Type::getVoidTy(Context),
        printIntArgType,
        false
    );
    Function *printInt = Function::Create(
        printIntDec,
        Function::ExternalLinkage,
        "printInt",
        TheModule.get()
    );

    Builder.CreateCall(printInt, rtn, "calltmp2");

    Builder.CreateRet(rtn);

    return main;
}

int emit_object() {
    TheModule = make_unique<Module>("llvmdemo", Context);

    InitializeNativeTarget();
    InitializeNativeTargetAsmParser();
    InitializeNativeTargetAsmPrinter();

    auto TargetTriple = sys::getDefaultTargetTriple();
    TheModule->setTargetTriple(TargetTriple);

    string Error;
    auto Target = TargetRegistry::lookupTarget(TargetTriple, Error);

    // Print an error and exit if we couldn't find the requested target.
    // This generally occurs if we've forgotten to initialise the
    // TargetRegistry or we have a bogus target triple.
    if (!Target) {
        errs() << Error;
        return 1;
    }

    auto CPU = "generic";
    auto Features = "";

    TargetOptions opt;
    auto RM = Optional<Reloc::Model>();
    auto TheTargetMachine =
            Target->createTargetMachine(TargetTriple, CPU, Features, opt, RM);

    TheModule->setDataLayout(TheTargetMachine->createDataLayout());

    // 生成IR
    Function *fun1 = codegen_fun1();
    codegen_main();

    auto Filename = "output.o";
    std::error_code EC;
    raw_fd_ostream dest(Filename, EC, sys::fs::OF_None);

    if (EC) {
        errs() << "Could not open file: " << EC.message();
        return 1;
    }

    legacy::PassManager pass;
    auto FileType = CGFT_ObjectFile;
    if (TheTargetMachine->addPassesToEmitFile(pass, dest, nullptr, FileType)) {
        errs() << "TheTargetMachine can't emit a file of this type";
        return 1;
    }

    dest.flush();
    outs() << "Wrote " << Filename << "\n";

    return 0;
}

int main(){
    // 编译成静态文件
    return emit_object();
}

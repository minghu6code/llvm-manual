cmake_minimum_required(VERSION 3.20)

project(llvm-manual CXX)

set (CMAKE_CXX_STANDARD 14)
set(CMAKE_BUILD_TYPE Debug)

# find Upper($lib-name)Config.cmake or Lower($lib-name)-config.cmake
# `REQUIRED` means it's essential for project,
# indicating exit if package not exists.
find_package(LLVM QUIET REQUIRED CONFIG)

message(STATUS "Found LLVMConfig.cmake in: ${LLVM_DIR}")
message(STATUS "LLVM VERSION ${LLVM_PACKAGE_VERSION}")
message(STATUS "LLVM Build Type: ${LLVM_BUILD_TYPE}")

message(STATUS "LLVM INCLUDE_DIRS: ${LLVM_INCLUDE_DIRS}")
message(STATUS "LLVM DEFINITIONS: ${LLVM_DEFINITIONS}")

# 设置全局参数
include_directories(${LLVM_INCLUDE_DIRS})
add_definitions(${LLVM_DEFINITIONS})

# 设定一个运行目标
set(MANUAL_LIST manual.cpp)
add_executable(llvm-manual ${MANUAL_LIST})

# 设置LLVM Link Component
llvm_map_components_to_libnames(llvm_libs
       support
       core
       irreader
       analysis
       ExecutionEngine
       InstCombine
       Object
       OrcJIT
       RuntimeDyld
       ScalarOpts
       native
)

target_link_libraries(llvm-manual ${llvm_libs})
